# awesome-sexy

This is my configuration for awesome-wm (works on debian and ubuntu)

# dependencies

This configuration defines shortcuts to call tools such as
- `xbacklight` to change screen backlight
- `slock` and `xtrlock` to lock the computer in different manner (display screen or not)
- `pulseaudio-utils` to change audio volume
- `dmenu` to start applications

# shortcuts

## global
|shortcut|effect|
|-|-|
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>ALT</kbd>+<kbd>SHIFT</kbd>+<kbd>q</kbd> | quit awesome-wm |
|<kbd>WIN</kbd>+<kbd>b</kbd> | toggle bottom bar |

## launchers
|shortcut|effect|
|-|-|
|<kbd>WIN</kbd>+<kbd>RETURN</kbd> or<br> <kbd>WIN</kbd>+<kbd>a</kbd> | launches a terminal |
|<kbd>WIN</kbd>+<kbd>ALT</kbd>+<kbd>RETURN</kbd> | launches a web browser |
|<kbd>WIN</kbd>+<kbd>ALT</kbd>+<kbd>SIHFT</kbd>+<kbd>RETURN</kbd> | launches a web browser with `--incognito` option |
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>RETURN</kbd> | launches a text editor |
|<kbd>WIN</kbd>+<kbd>s</kbd> | launches a `pactl` |
|<kbd>WIN</kbd>+<kbd>r</kbd> | launches a `dmenu` |
|<kbd>WIN</kbd>+<kbd>e</kbd> | launches `dolphin` |
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>s</kbd> | launches a `alsamixer` in the default terminal |
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>DEL</kbd> | launches `htop` in the default terminal |

## desktop specific
|shortcut|effect|
|-|-|
|<kbd>WIN</kbd>+<kbd>1 2 3 4 5 6 7 8 9 0 - =</kbd> | goto desktop 1 2 3 4 5 6 7 8 9 0 - = |
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>1 2 3 4 5 6 7 8 9 0 - =</kbd> | move window to desktop 1 2 3 4 5 6 7 8 9 0 - = |
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>RETURN</kbd> | selected window set to main window |
|<kbd>WIN</kbd>+<kbd>SPACE</kbd> | change layout (there are 4 modes) |
|<kbd>WIN</kbd>+<kbd>,</kbd> or<br> <kbd>WIN</kbd>+<kbd>.</kbd> | move separation line between main window(s) and auxiliary window(s) to the left/right |
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>,</kbd> or<br> <kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>.</kbd> | split auxiliary windows into more/less columns |
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>,</kbd> or<br> <kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>.</kbd> | add/reduce main window capacity |
|<kbd>WIN</kbd>+<kbd>j</kbd> or<br> <kbd>WIN</kbd>+<kbd>k</kbd> or<br><kbd>WIN</kbd>+<kbd>TAB</kbd> or<br> <kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>TAB</kbd> | changes focus to next/previous window |


## window specific
|shortcut|effect|
|-|-|
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>c</kbd> | close window |
|<kbd>WIN</kbd>+<kbd>SHIFT</kbd>+<kbd>f</kbd> | no-fullscreen |
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>f</kbd> | mega-fullscreen |
|<kbd>WIN</kbd>+<kbd>f</kbd> | fullscreen |
|<kbd>WIN</kbd>+<kbd>CTRL</kbd>+<kbd>SPACE</kbd> | floating mode |
|<kbd>WIN</kbd>+<kbd>t</kbd> | toggle always on top |
