local awful = require("awful")

local function window_cycle(indexshift,follow)
	return function(c)
		local curidx = awful.tag.getidx()
		curidx = curidx + indexshift
		while curidx<=0 do curidx = curidx + workspace_count end
		while curidx>workspace_count do curidx = curidx - workspace_count end
		if client.focus then
			local tag = awful.tag.gettags(client.focus.screen)[curidx]
			if tag then awful.client.movetotag(tag) end
		end
		if follow then awful.tag.viewidx(indexshift) end
	end
end

local function fullscreen_switch(full,max,float)
	return function(c)
		if full or max then float = false end
		c.fullscreen = full
		c.maximized = max
		c.floating=float
		if max then c.border_width = 0 else c.border_width = beautiful.border_width end
		-- c.border_width = beautiful.border_width
		c:raise()
	end
end

return awful.util.table.join(
	--Move Window to Workspace Left/Right
	 awful.key({ modkey, "Control" ,"Shift" }, "Left" , window_cycle(-1,false), {description = "throw window to prev workspace", group = "client"})
	,awful.key({ modkey, "Control" ,"Shift" }, "Right", window_cycle( 1,false), {description = "throw window to next workspace", group = "client"})
	,awful.key({ modkey,            "Shift" }, "Left" , window_cycle(-1,true) , {description = "move window to prev workspace" , group = "client"})
	,awful.key({ modkey,            "Shift" }, "Right", window_cycle( 1,true) , {description = "move window to next workspace" , group = "client"})

	,awful.key({ modkey, "Shift"   }, "f", fullscreen_switch(false,false,false), {description = "fullscreen disable", group = "client"})
	,awful.key({ modkey, altkey    }, "f", fullscreen_switch(false,false,true ), {description = "float"             , group = "client"})
	,awful.key({ modkey, "Control" }, "f", fullscreen_switch(true ,false), {description = "fullscreen enable" , group = "client"})
	,awful.key({ modkey,           }, "f", fullscreen_switch(false,true ), {description = "maximize"          , group = "client"})
	-- ,awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     , {description = "toggle floating", group = "client"})

	,awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end, {description = "close", group = "client"})
	,awful.key({ modkey, "Shift"   }, "Return", function (c) c:swap(awful.client.getmaster()) end, {description = "move to master", group = "client"})
	,awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end, {description = "move to screen", group = "client"})
	,awful.key({ modkey, "Shift"   }, "t",      function (c) c.ontop = false                  end, {description = "toggle keep on top", group = "client"})
	,awful.key({ modkey,           }, "t",      function (c) c.ontop = true                   end, {description = "toggle keep on top", group = "client"})
)
