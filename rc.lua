local initialization = require("rc-init") -- ! this must be done first

require("awful.autofocus")
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")

awful.spawn.with_shell("startup")

beautiful = require("beautiful")
beautiful.init(awful.util.getdir("config") .. "/themes/BSOD/theme.lua")
-- beautiful.init(awful.util.getdir("config") .. "/themes/xp_theme/theme.lua")
-- beautiful.init(awful.util.get_themes_dir() .. "default/theme.lua")

backlight  = require("widget/backlight")
volume     = require("widget/volume")
screenshot = require("widget/screenshot")

keyboard = require("widget/keyboard")
keyboard.set_layout('sexykbd')

local battery    = require("widget/battery")
local cpu        = require("widget/cpu")
local ram        = require("widget/ram")
local swap       = require("widget/swap")
local separator  = require("widget/separator")

local clock = wibox.widget.textclock(" %a %d-%m-%Y %H:%M:%S ",1)

terminal = "x-terminal-emulator"
text_editor_1   = "sub"
text_editor_2 = "code"
-- editor_cmd = terminal .. " -e " .. tostring(os.getenv("EDITOR"))

browser           = "google-chrome"
browser_incognito = "google-chrome --incognito"
-- browser           = "chromium-browser"
-- browser_incognito = "chromium-browser --incognito"
-- browser           = "firefox"
-- browser_incognito = "firefox --private-window"

modkey = "Mod4"
altkey = "Mod1"
altkeyR = "Mod5"

awful.layout.layouts = {
	awful.layout.suit.tile,
	awful.layout.suit.floating,
	awful.layout.suit.tile.bottom,
	awful.layout.suit.floating,
	-- awful.layout.suit.max,
	-- awful.layout.suit.tile.left,
	-- awful.layout.suit.tile.top,
	-- awful.layout.suit.fair,
	-- awful.layout.suit.fair.horizontal,
	-- awful.layout.suit.spiral,
	-- awful.layout.suit.spiral.dwindle,
	-- awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
	-- awful.layout.suit.max.fullscreen,
	-- awful.layout.suit.magnifier,
}

-- {{{ Helper functions
	local function client_menu_toggle_fn()
		local instance = nil

		return function ()
			if instance and instance.wibox.visible then
				instance:hide()
				instance = nil
			else
				instance = awful.menu.clients({ theme = { width = 250 } })
			end
		end
	end

	local function set_wallpaper(s)
		-- Wallpaper
		if beautiful.wallpaper then
			local wallpaper = beautiful.wallpaper
			-- If wallpaper is a function, call it with the screen
			if type(wallpaper) == "function" then
				wallpaper = wallpaper(s)
			end
			gears.wallpaper.maximized(wallpaper, s, true)
		end
	end
-- }}}



-- {{{ Wibar
	-- Create a wibox for each screen and add it
	local taglist_buttons = awful.util.table.join(
		awful.button({}, 1, function(t) t:view_only() end)
	)

	local tasklist_buttons = awful.util.table.join(
		awful.button({}, 1,
			function(c)
				client.focus = c
				c:raise()
			end
		),
		awful.button({}, 3, client_menu_toggle_fn())
	)

	screen.connect_signal("property::geometry", set_wallpaper)

	tags = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "="}
	workspace_count = #tags

	awful.screen.connect_for_each_screen(function(s)
		-- Wallpaper
		set_wallpaper(s)

		-- Each screen has its own tag table.
		awful.tag(tags, s, awful.layout.layouts[1])

		s.mylayoutbox = awful.widget.layoutbox(s)
		s.mytaglist   = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)
		s.mytasklist  = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

		s.mywibox     = awful.wibar({ position = "bottom", height = 15, screen = s })
		s.mywibox:setup {
			layout = wibox.layout.align.horizontal,
			-- Left widgets
			{
				layout = wibox.layout.fixed.horizontal,
				s.mytaglist,
				separator, cpu,
				separator, ram,
				separator, swap,
				separator,
			},
			
			-- Middle widget
			s.mytasklist,
			
			-- Right widgets
			{
				layout = wibox.layout.fixed.horizontal,
				separator, wibox.widget.systray(),
				separator, volume.widget,
				separator, battery,
				separator, clock,
				separator, s.mylayoutbox,
			},
		}

	end)
-- }}}


clientbuttons = awful.util.table.join(
	awful.button({}, 1, function (c) client.focus = c; c:raise() end),
	awful.button({ modkey }, 1, awful.mouse.client.move),
	awful.button({ modkey }, 3, awful.mouse.client.resize))

globalkeys = require("shortcuts_global")
root.keys(globalkeys)

clientkeys = require("shortcuts_client")

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{ rule = {},
	  properties = { border_width = beautiful.border_width,
					 border_color = beautiful.border_normal,
					 focus = awful.client.focus.filter,
					--  raise = true,
					 raise = false,
					 keys = clientkeys,
					 buttons = clientbuttons,
					 screen = awful.screen.preferred,
					 placement = awful.placement.no_overlap+awful.placement.no_offscreen
	 }
	},

	-- Floating clients.
	{ rule_any = {
		instance = {
			"DTA",  -- Firefox addon DownThemAll.
			"copyq",  -- Includes session name in class.
			"guake",  -- guake
		},
		class = {
			"Arandr",
			"Gpick",
			"Kruler",
			"MessageWin",  -- kalarm.
			"Sxiv",
			"Wpa_gui",
			"pinentry",
			"veromix",
			"xtightvncviewer"},
		name = {
			"Event Tester",  -- xev.
			"Blender User Preferences",  -- blender ctrl+alt+u
			"Blender Preferences",  -- blender ctrl+alt+u
			"Blender Render",  -- blender ctrl+alt+u
			"Blender",  -- blender ctrl+alt+u
		},
		role = {
			"AlarmWindow",  -- Thunderbird's calendar.
			"pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
		}
	  }, properties = { floating = true }},

	-- Set Firefox to always map on the tag named "2" on screen 1.
	-- { rule = { class = "Firefox" }, properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	-- if not awesome.startup then awful.client.setslave(c) end

	if awesome.startup and
	  not c.size_hints.user_position
	  and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
		and awful.client.focus.filter(c) then
		client.focus = c
	end
end)

client.connect_signal("focus",   function(c) c.border_color = beautiful.border_focus  end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
