local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local naughty = require("naughty")

local function client_focus(shift) return function() awful.client.focus.byidx(shift) end end
local function client_swap(shift) return function() awful.client.swap.byidx(shift) end end

local function viewWorkspace(i)
	return function()
		local screen = awful.screen.focused()
		local tag = screen.tags[i]
		if tag then tag:view_only() end
	end
end

local function moveClientToWorkspace(i)
	return function()
		if client.focus then
			local tag = client.focus.screen.tags[i]
			if tag then client.focus:move_to_tag(tag) end
		end
	end
end

local globalkeys = {}

for k,v in pairs(tags) do
	globalkeys = awful.util.table.join(globalkeys,
		awful.key({ modkey          }, v, viewWorkspace(k),         {description = "view workspace "                  ..v, group = "tag"}),
		awful.key({ modkey, "Shift" }, v, moveClientToWorkspace(k), {description = "move focused client to workspace "..v, group = "tag"})
	)
end

globalkeys = awful.util.table.join(globalkeys,

	awful.key({ modkey }, "h",   hotkeys_popup.show_help, {description="show help", group="awesome"}),

	awful.key({ modkey, "Control", "Shift", altkey }, "q", awesome.quit   , {description = "quit awesome"  , group = "awesome"}),
	awful.key({ modkey, "Control"                  }, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),

	-----------------------------------------------------------------------------------------------------

	awful.key({}, "XF86AudioMute"        ,volume.mute),
	awful.key({}, "XF86AudioLowerVolume" ,volume.down),
	awful.key({}, "XF86AudioRaiseVolume" ,volume.up  ),
	awful.key({}, "XF86AudioMicMute"     ,volume.full),

	awful.key({}, "XF86MonBrightnessDown",backlight.minus),
	awful.key({}, "XF86MonBrightnessUp"  ,backlight.plus),

	-- awful.key({}       , "XF86Launch1", function () awful.spawn_with_shell("moo") end),

	-----------------------------------------------------------------------------------------------------

	awful.key({modkey},"F1",volume.mute,{description="volume mute", group="F-keys"}),
	awful.key({modkey},"F2",volume.down,{description="volume down", group="F-keys"}),
	awful.key({modkey},"F3",volume.up  ,{description="volume up  ", group="F-keys"}),
	awful.key({modkey},"F4",volume.full,{description="volume full", group="F-keys"}),

	awful.key({modkey},"F5",backlight.min  ,{description="backlight min"     ,group="F-keys"}),
	awful.key({modkey},"F6",backlight.minus,{description="backlight decrease",group="F-keys"}),
	awful.key({modkey},"F7",backlight.plus ,{description="backlight increase",group="F-keys"}),
	awful.key({modkey},"F8",backlight.max  ,{description="backlight max"     ,group="F-keys"}),



-- TODO mute/enable microphone/webcam



	-- awful.key({      },"Print" ,function() awful.spawn.with_shell("screenshot",false) end),
	awful.key({modkey},"Home"  ,function() awful.spawn.with_shell("xcalib -invert -alter") end,{description="invert color", group = "F-keys"}),
	awful.key({modkey},"End"   ,screenshot.interactive("/home/doek/scrot/")        ,{description="interactive screenshot", group = "F-keys"}),
	awful.key({modkey},"Insert",screenshot.saveFull("/home/doek/scrot/")           ,{description="screenshot → /home/doek/scrot/Y-M-D-h:m:s", group = "F-keys"}),
	awful.key({modkey},"Delete",screenshot.clipboard                               ,{description="screenshot → clipboard", group = "F-keys"}),

	-----------------------------------------------------------------------------------------------------

	awful.key({modkey,"Shift"},"F1",function()keyboard.set_layout('sexykbd') end),
	awful.key({modkey,"Shift"},"F2",function()keyboard.set_layout('jp') end),
	awful.key({modkey,"Shift"},"F3",function()keyboard.set_layout('ch fr') end),
	awful.key({modkey,"Shift"},"F4",function()keyboard.set_layout('apl') end),

	awful.key({modkey,"Shift"},"F5",function() keyboard.set_layout("fr") end),
	awful.key({modkey,"Shift"},"F6",function() keyboard.set_layout("fr bepo") end),
	awful.key({modkey,"Shift"},"F7",function() keyboard.set_layout("ru") end),
	awful.key({modkey,"Shift"},"F8",function() keyboard.set_layout("gr") end),
	-- awful.key({modkey,"Shift"},"F7",function() keyboard.set_layout("us mac") end),

	-- awful.key({modkey,"Shift"},"F9" ,function() keyboard.set_layout("us dvp") end),
	-- awful.key({modkey,"Shift"},"F10",function() keyboard.set_layout("us dvorak-l") end),
	-- awful.key({modkey,"Shift"},"F11",function() keyboard.set_layout("us dvorak-r") end),
	-- awful.key({modkey,"Shift"},"F12",function() keyboard.set_layout("us dvorak-alt-intl") end),

	-----------------------------------------------------------------------------------------------------

	awful.key({modkey},"b",function() mouse.screen.mywibox.visible = not mouse.screen.mywibox.visible end),
	awful.key({modkey},"r",function() awful.spawn.with_shell("dmenu_run") end, {description = "open dmenu_run", group = "launcher"}),
	-- awful.key({modkey},"r",function() awful.screen.focused().mypromptbox:run() end, {description = "run prompt", group = "launcher"}),

	-----------------------------------------------------------------------------------------------------

	awful.key({modkey,       },"w",function() naughty.notify({text="touch → disable"}) awful.spawn.with_shell('xinput --disable "Wacom Pen and multitouch sensor Finger touch"') end, {description = "disable touch", group = "interaction"}),
	awful.key({modkey,"Shift"},"w",function() naughty.notify({text="touch → enable" }) awful.spawn.with_shell('xinput --enable  "Wacom Pen and multitouch sensor Finger touch"') end, {description = "enable touch", group = "interaction"}),

	awful.key({modkey,"Shift"},"a",function() naughty.notify({text="middle → scroll"}) awful.spawn.with_shell("trackpoint_scroll_enable" ) end , {description = "trackpoint middle button = scroll"    , group = "mouse configuration"}),
	awful.key({modkey,       },"a",function() naughty.notify({text="middle → btn"})    awful.spawn.with_shell("trackpoint_scroll_disable") end , {description = "trackpoint middle button = middle btn", group = "mouse configuration"}),

	-- TODO : quake like console
	-- ,awful.key({ modkey,           }, "`",  aaaaaaaaaaaaaaaaaaaaaaaaaaaa(1))
	-- ,awful.key({ modkey, "Shift"   }, "`",  aaaaaaaaaaaaaaaaaaaaaaaaaaaa(-1))

	-- awful.key({ modkey                     }, "a", function() awful.spawn.with_shell( terminal ) end),
	awful.key({modkey                  },"Return",function() awful.spawn.with_shell(terminal) end                   ,{description="open terminal"     ,group="launcher"}),
	awful.key({modkey,"Control"        },"Delete",function() awful.spawn.with_shell(terminal .. " -e htop") end     ,{description="open htop"         ,group="launcher"}),
	awful.key({modkey,"Shift"          },"s"     ,function() awful.spawn.with_shell(terminal .. " -e alsamixer") end,{description="open alsamixer"    ,group="launcher"}),
	awful.key({modkey                  },"s"     ,function() awful.spawn.with_shell("pavucontrol") end              ,{description="open pavucontrol"  ,group="launcher"}),
	awful.key({modkey,"Control"        },"Return",function() awful.spawn.with_shell(text_editor_1) end              ,{description="open text editor 1",group="launcher"}),
	awful.key({modkey,"Control","Shift"},"Return",function() awful.spawn.with_shell(text_editor_2) end              ,{description="open text editor 2",group="launcher"}),
	awful.key({modkey, altkey          },"Return",function() awful.spawn.with_shell(browser) end                    ,{description="open www-browser"  ,group="launcher"}),
	awful.key({modkey, altkeyR         },"Return",function() awful.spawn.with_shell(browser) end),
	awful.key({modkey, altkey ,"Shift" },"Return",function() awful.spawn.with_shell(browser_incognito) end          ,{description="open www-browser incognito",group="launcher"}),
	awful.key({modkey, altkeyR,"Shift" },"Return",function() awful.spawn.with_shell(browser_incognito) end),
	awful.key({modkey,                 },"e"     ,function() awful.spawn.with_shell("dolphin") end                  ,{description="open a terminal"   ,group="launcher"}),

	awful.key({modkey                  },"Escape",function() awful.spawn.with_shell("slock") end                    ,{description="lock"                 ,group="lock"}),
	awful.key({modkey,"Shift"          },"Escape",function() awful.spawn.with_shell("xtrlock") end                  ,{description="lock but show display",group="lock"}),

	awful.key({modkey,                 },"Left", awful.tag.viewprev,{description="view prev",group="tag"}),
	awful.key({modkey,                 },"Right",awful.tag.viewnext,{description="view next",group="tag"}),

	awful.key({modkey,                 },"Tab",  client_focus( 1),{description="focus next",group="client"}),
	awful.key({modkey,"Shift"          },"Tab",  client_focus(-1),{description="focus prev",group="client"}),
	awful.key({modkey,                 },"Next", client_focus( 1),{description="focus next",group="client"}),
	awful.key({modkey,                 },"Prior",client_focus(-1),{description="focus prev",group="client"}),

	awful.key({modkey,"Shift"          }, "Next", client_swap( 1),{description="swap with next client by index"    ,group="client"}),
	awful.key({modkey,"Shift"          }, "Prior",client_swap(-1),{description="swap with previous client by index",group="client"}),

	awful.key({modkey,                 }, ".",function() awful.tag.incmwfact( 0.05)         end,{description="increase master width factor"         ,group="layout"}),
	awful.key({modkey,                 }, ",",function() awful.tag.incmwfact(-0.05)         end,{description="decrease master width factor"         ,group="layout"}),
	awful.key({modkey,"Control"        }, ",",function() awful.tag.incnmaster( 1, nil, true)end,{description="increase the number of master clients",group="layout"}),
	awful.key({modkey,"Control"        }, ".",function() awful.tag.incnmaster(-1, nil, true)end,{description="decrease the number of master clients",group="layout"}),
	awful.key({modkey,"Shift"          }, ",",function() awful.tag.incncol( 1, nil, true)   end,{description="increase the number of columns"       ,group="layout"}),
	awful.key({modkey,"Shift"          }, ".",function() awful.tag.incncol(-1, nil, true)   end,{description="decrease the number of columns"       ,group="layout"}),

	awful.key({modkey,                 },"space",function() awful.layout.inc( 1)            end,{description="select next"    , group="layout"}),
	awful.key({modkey,"Shift"          },"space",function() awful.layout.inc(-1)            end,{description="select previous", group="layout"}),

	-- awful.key({ modkey, "Control", "Shift", altkey }, "h", function() awful.spawn.with_shell('heboris') end, {description = "PROCRASTINATE"  , group = "procrastination"}),

	--rotate screen
	awful.key({modkey,"Control",altkey},"Up",   function() awful.spawn.with_shell("rotate_screen normal") end),
	awful.key({modkey,"Control",altkey},"Left", function() awful.spawn.with_shell("rotate_screen left") end),
	awful.key({modkey,"Control",altkey},"Right",function() awful.spawn.with_shell("rotate_screen right") end),
	awful.key({modkey,"Control",altkey},"Down", function() awful.spawn.with_shell("rotate_screen inverted") end)

)

return globalkeys
