local naughty = require("naughty")
local awful = require("awful")

local keyboard = {}

keyboard.set_layout = function(keyboard_layout)
	awful.spawn.with_shell('setxkbmap_laurent '..keyboard_layout)
	-- awful.spawn.with_shell('xset r rate 135 110')
	-- awful.spawn.with_shell('ibus engine "xkb:us::eng"')
	-- awful.spawn.with_shell('setxkbmap ' .. keyboard_layout)
	-- awful.spawn.with_shell('setxkbmap -option "caps:swapescape"')
	-- awful.spawn.with_shell('xmodmap -e "keycode 135 = Super_R"')
	-- awful.spawn.with_shell('xmodmap -e "keycode 230 = Menu"')
	-- if keyboard_layout == 'jp' then awful.spawn.with_shell("ibus engine anthy") end
	naughty.notify({ text = "keyboard layout → ".. keyboard_layout })
end

keyboard.set_layout('sexykbd')

return keyboard
