local wibox = require("wibox")

local separator
-- separator = wibox.widget.textbox()
-- separator:set_text(" ")
separator = wibox.widget.progressbar()
separator:set_width(2)
separator:set_background_color("#000000")

return separator
