local wibox = require("wibox")
local vicious = require("vicious")

local widget_t = wibox.widget.textbox()
local widget_g = wibox.widget.progressbar()

local widget = wibox.widget {
	layout = wibox.layout.stack,
	{
		-- paddings      = 1,
		-- border_width  = 1,
		-- border_color  = beautiful.border_color,
		width         = 300,
		widget        = widget_g,
		color         = "#FF0000"
	},
	{
		align  = 'center',
		widget = widget_t,
	}
}

local delta_t = 10

vicious.register(widget_t, vicious.widgets.bat, 
	function(w,a) return '<span fgcolor="#000000"> ' .. a[2] .. '% ' .. a[1] .. a[3] ..'</span>' end, delta_t, "BAT0"
)

vicious.register(widget_g, vicious.widgets.bat,
	function (w,a)
		if a[1] == "+" then
			widget_g:set_color("#00FF00")
			widget_g:set_background_color(beautiful.bg_widget)
		else
			if a[2] < 10 then
				widget_g:set_color("#FF0000")
				widget_g:set_background_color("#AA0000")
			else
				-- widget_g:set_color("#008800")
				widget_g:set_color(beautiful.bg_focus)
				widget_g:set_background_color(beautiful.bg_widget)
			end
		end
		return a[2]
	end,
	delta_t, "BAT0"
)

return widget
