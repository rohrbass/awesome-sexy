local graph_widget = require("widget/widget_template/graph")
local vicious = require("vicious")

return graph_widget(
	function( widget_t, widget_g )
		vicious.register(widget_g, vicious.widgets.cpu, 
		function (widget, args)
			local percentage = string.format("%02d", args[1])
			widget_t:set_text("π ".. percentage .."%")
			return args[1]
		end,2)
	end
)
