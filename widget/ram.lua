local graph_widget = require("widget/widget_template/graph")
local vicious = require("vicious")

return graph_widget(
	function( widget_t, widget_g )
		vicious.register(widget_g, vicious.widgets.mem,
		function (widget, args)
			local percentage = math.floor(args[2]/args[3]*1000)/10
			widget_t:set_text("μ "..percentage .."%")
			return args[1]
		end,2)
	end
)
