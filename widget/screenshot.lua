local awful = require("awful")

local screenshot = {}

screenshot.saveFull = function(path)
	return function()
		awful.spawn("mkdir -p " .. path)
		awful.spawn("scrot -e 'mv $f "..path.."' %Y-%m-%d-%T-screenshot.png")
	end
end

screenshot.clipboard = function() awful.spawn("flameshot full -c") end

screenshot.interactive = function(path)
	return function() awful.spawn("flameshot gui -p "..path) end
end

return screenshot