local graph_widget = require("widget/widget_template/graph")
local vicious = require("vicious")

return graph_widget(
	function( widget_t, widget_g )
		vicious.register(widget_g, vicious.widgets.mem,
		function (widget, args)
			local percentage = math.floor(args[6]/args[7]*1000)/10
			widget_t:set_text("swap "..percentage .."%")
			return args[5]
		end,2)
	end
)
