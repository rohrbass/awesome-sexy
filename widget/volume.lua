local wibox = require("wibox")
local awful = require("awful")

local function clamp(x,min,max)
	if x < min then return min end
	if x > max then return max end
	return x
end

local volume = {}

local widget_t = wibox.widget.textbox()
local widget_g = wibox.widget.progressbar()
local widget = wibox.widget {
	layout = wibox.layout.stack,
	{
		-- paddings      = 1,
		-- border_width  = 1,
		-- border_color  = beautiful.border_color,
		background_color = beautiful.bg_widget,
		color = "#884422",
		width = 100,
		border_color  = "#ff0000",
		widget        = widget_g,
	},
	{
		align  = 'center',
		widget = widget_t,
	}
}

local V = 0
local V_max = 500

volume.widget = widget

volume.set = function(volume)
	V = clamp(volume,0,V_max)
	awful.spawn.with_shell("for i in $(pactl list|grep ^Sink|grep -o '[0-9]') ; do  pactl set-sink-volume $i " .. V .. "% ; done")
	volume_str = string.format("%02d", V)
	widget_t:set_text("♬ " .. volume_str .. " ♬")
	widget_g:set_value(volume_str/100.)
end

volume.increment = function(increment) volume.set(V + increment) end

volume.mute = function() volume.set(0)        end
volume.up   = function() volume.increment( 5) end
volume.down = function() volume.increment(-5) end
volume.full = function() volume.set(100)      end

-- volume.mute() -- on startup, mute sound

return volume
