local wibox = require("wibox")

function graph_widget( register_function )
	local widget_t
	local widget_g
	
	widget_t = wibox.widget.textbox()
	widget_g = wibox.widget.graph()
	widget_g:set_width(100)
	widget_g:set_background_color("#494B4F")
	widget_g:set_color(beautiful.bg_focus)

	register_function( widget_t, widget_g )
	
	local widget = wibox.widget {
		{
			-- max_value     = 1,
			-- value         = 0.5,
			-- forced_height = 20,
			-- paddings      = 1,
			-- border_width  = 1,
			-- border_color  = beautiful.border_color,
			widget        = widget_g,
		},
		{
			align  = 'center',
			-- font   = 'mono 10',
			widget = widget_t,
		},
		layout = wibox.layout.stack
	}
	return widget
end

return graph_widget
