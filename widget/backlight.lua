
local awful = require("awful")

local function clamp(x,min,max)
	if x < min then return min end
	if x > max then return max end
	return x
end

local function size(array)
	local out = 0 for k, v in pairs(array) do out = out + 1 end return out
end

local backlight = {}
local backlight_value = 100

local B_values = {0,1,2,4,6,8,10,15,20,30,40,60,80,100}
local B_max = size(B_values)
local B = B_max

backlight.set = function(b)
	B = clamp(b,1,B_max)
	awful.spawn("xbacklight -steps 1 -set " .. tostring(B_values[B]), false)
end

backlight.plus  = function() backlight.set(B+1)   end
backlight.minus = function() backlight.set(B-1)   end
backlight.min   = function() backlight.set(2)     end
backlight.max   = function() backlight.set(B_max) end

return backlight