theme = {}

-- theme.font          = "fixed 7"
theme.font          = "mono 8"

theme.bg_normal     = "#191919"
theme.bg_focus      = "#ea841e"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"

theme.fg_normal     = "#ffffff"
theme.fg_focus      = "#000000"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.border_width  = "1"
theme.border_normal = "#333333"
theme.border_focus  = "#ea841e"
theme.border_marked = "#91231c"

awful = require("awful")
path = awful.util.getdir("config") .. "/themes/laurent_theme"


-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
-- theme.taglist_bg_focus = "#ff0000"

-- Display the taglist squares
-- theme.taglist_squares_sel   = path .. "/taglist/squarefw.png"
theme.taglist_squares_sel   = path .. "/taglist/squarew3_.png"
theme.taglist_squares_unsel = path .. "/taglist/squarew3.png"

-- theme.tasklist_floating_icon = path .. "/tasklist/floatingw.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = path .. "/submenu.png"
theme.menu_height = "15"
theme.menu_width  = "170"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
-- theme.bg_widget = "#cc0000"

--====================================================================================================
-- theme.wallpaper = path .. "/bliss.png"
-- theme.wallpaper = path .. "/start_bg.png"
-- theme.wallpaper = path .. "/start_bg2.png"
-- theme.wallpaper = path .. "/BSOD.png"
-- theme.wallpaper = path .. "/win_xp_start.png"
-- theme.wallpaper = path .. "/win_xp.png"
-- theme.wallpaper = path .. "/win95_.png"
-- theme.wallpaper = path .. "/win95_chain.png"
-- theme.wallpaper = path .. "/win95_chain_big.png"
-- theme.wallpaper = path .. "/win95_chain_medium.png"
-- theme.wallpaper = path .. "/win95_chain_medium2.png"

-- theme.wallpaper = path .. "/mice.png"

-- theme.wallpaper = path .. "/windows_BG_setp16.jpg"
-- theme.wallpaper = path .. "/win95_chain_medium3.png"
-- theme.wallpaper = path .. "/win95_desktop2.png"
theme.wallpaper = path .. "/win95_desktop3.png"
-- theme.wallpaper = path .. "/win95_bg.png"

--====================================================================================================
-- You can use your own layout icons like this:

theme.layout_fairh =      path ..      "/layouts/fairhw.png"
theme.layout_fairv =      path ..      "/layouts/fairvw.png"
theme.layout_floating  =  path ..   "/layouts/floatingw.png"
theme.layout_magnifier =  path ..  "/layouts/magnifierw.png"
theme.layout_max =        path ..        "/layouts/maxw.png"
theme.layout_fullscreen = path .. "/layouts/fullscreenw.png"
theme.layout_tilebottom = path .. "/layouts/tilebottomw.png"
theme.layout_tileleft   = path ..   "/layouts/tileleftw.png"
theme.layout_tile =       path ..       "/layouts/tilew.png"
theme.layout_tiletop =    path ..    "/layouts/tiletopw.png"
theme.layout_spiral  =    path ..     "/layouts/spiralw.png"
theme.layout_dwindle =    path ..    "/layouts/dwindlew.png"

-- theme.layout_fairh =      path ..      "/layouts/fairh.png"
-- theme.layout_fairv =      path ..      "/layouts/fairv.png"
-- theme.layout_floating  =  path ..   "/layouts/floating.png"
-- theme.layout_magnifier =  path ..  "/layouts/magnifier.png"
-- theme.layout_max =        path ..        "/layouts/max.png"
-- theme.layout_fullscreen = path .. "/layouts/fullscreen.png"
-- theme.layout_tilebottom = path .. "/layouts/tilebottom.png"
-- theme.layout_tileleft   = path ..   "/layouts/tileleft.png"
-- theme.layout_tile =       path ..       "/layouts/tile.png"
-- theme.layout_tiletop =    path ..    "/layouts/tiletop.png"
-- theme.layout_spiral  =    path ..     "/layouts/spiral.png"
-- theme.layout_dwindle =    path ..    "/layouts/dwindle.png"

theme.awesome_icon = path .. "/start.png"

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
