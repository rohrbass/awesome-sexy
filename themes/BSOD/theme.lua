-- {{{ Main
local theme = {}
local awful = require("awful")
local path = awful.util.getdir("config") .. "/themes/BSOD/"
theme.wallpaper = path .. "win95_desktop.png"
theme.wallpaper = path .. "Windows_9X_BSOD.png"

-- }}}

-- {{{ Styles
theme.font      = "mono 9"

-- {{{ Colors
	theme.bg_normal     = "#191919"
	theme.bg_focus      = "#ea841e"
	theme.bg_urgent     = "#ff0000"
	-- theme.bg_normal  = "#3F3F3F"
	-- theme.bg_focus   = "#1E2320"
	-- theme.bg_urgent  = "#3F3F3F"

	theme.fg_normal     = "#ffffff"
	theme.fg_focus      = "#000000"
	theme.fg_urgent     = "#ffffff"
	-- theme.fg_normal  = "#DCDCCC"
	-- theme.fg_focus   = "#F0DFAF"
	-- theme.fg_urgent  = "#CC9393"

	theme.bg_minimize   = "#444444"
	theme.fg_minimize   = "#ffffff"

	theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
	theme.useless_gap   = 0
	theme.border_width  = 2
	-- theme.border_width  = 0
	-- theme.border_normal = "#333333"
	theme.border_normal = "#000000"
	theme.border_focus  = "#ea841e"
	theme.border_marked = "#91231c"
	-- theme.border_normal = "#3F3F3F"
	-- theme.border_focus  = "#6F6F6F"
	-- theme.border_marked = "#CC9393"
-- }}}

-- {{{ Titlebars
-- theme.titlebar_bg_focus  = "#3F3F3F"
-- theme.titlebar_bg_normal = "#3F3F3F"
theme.titlebar_bg_focus  = "#ea841e"
theme.titlebar_bg_normal = "#333333"
-- }}}

-- theme.taglist_bg_focus      =
-- theme.taglist_fg_focus      =
-- theme.taglist_bg_urgent     =
-- theme.taglist_fg_urgent     =

-- theme.tasklist_bg_focus     =
-- theme.tasklist_fg_focus     =
-- theme.tasklist_bg_urgent    =
-- theme.tasklist_fg_urgent    =

-- theme.titlebar_normal =
-- theme.titlebar_focus  =

-- theme.tooltip_font          =
-- theme.tooltip_opacity       =
-- theme.tooltip_fg_color      =
-- theme.tooltip_bg_color      =
-- theme.tooltip_border_width  =
-- theme.tooltip_border_color  =


-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
theme.fg_widget        = "#AECF96"
theme.fg_center_widget = "#88A175"
theme.fg_end_widget    = "#FF5656"
theme.bg_widget        = "#494B4F"
theme.border_widget    = "#3F3F3F"
-- }}}

-- theme.menu_bg_normal    =
-- theme.menu_bg_focus     =
-- theme.menu_fg_normal    =
-- theme.menu_fg_focus     =
-- theme.menu_border_color =
-- theme.menu_border_width =
theme.menu_height = 15
theme.menu_width  = 170

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = path .. "taglist/squarefz.png"
theme.taglist_squares_unsel = path .. "taglist/squarez.png"
-- theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
-- theme.awesome_icon           = path .. "awesome-icon.png"
-- theme.menu_submenu_icon      = path .. "submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = path .. "layouts/tile.png"
theme.layout_tileleft   = path .. "layouts/tileleft.png"
theme.layout_tilebottom = path .. "layouts/tilebottom.png"
theme.layout_tiletop    = path .. "layouts/tiletop.png"
theme.layout_fairv      = path .. "layouts/fairv.png"
theme.layout_fairh      = path .. "layouts/fairh.png"
theme.layout_spiral     = path .. "layouts/spiral.png"
theme.layout_dwindle    = path .. "layouts/dwindle.png"
theme.layout_max        = path .. "layouts/max.png"
theme.layout_fullscreen = path .. "layouts/fullscreen.png"
theme.layout_magnifier  = path .. "layouts/magnifier.png"
theme.layout_floating   = path .. "layouts/floating.png"
theme.layout_cornernw   = path .. "layouts/cornernw.png"
theme.layout_cornerne   = path .. "layouts/cornerne.png"
theme.layout_cornersw   = path .. "layouts/cornersw.png"
theme.layout_cornerse   = path .. "layouts/cornerse.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus               = path .. "titlebar/close_focus.png"
theme.titlebar_close_button_normal              = path .. "titlebar/close_normal.png"

theme.titlebar_minimize_button_normal           = path .. "titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = path .. "titlebar/minimize_focus.png"

theme.titlebar_ontop_button_focus_active        = path .. "titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = path .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = path .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = path .. "titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active       = path .. "titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = path .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = path .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = path .. "titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active     = path .. "titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = path .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = path .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = path .. "titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active    = path .. "titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = path .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = path .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = path .. "titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
